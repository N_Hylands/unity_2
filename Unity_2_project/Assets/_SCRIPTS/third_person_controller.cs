﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class third_person_controller : MonoBehaviour {



	/*
	 * Third Person Camera Controller
	 * By Nicholas Hylands
	 * 
	 * -----------------
	 * 
	 * to-do:
	 * 
	 * 	Mecanim/animation controller support
	 * 	
	 * 
	 * 
	 * 
	*/

	public Camera main_camera; //we'll let the user create their own camera, and use that, so that they can use it easier.

	public bool turn_off_other_cameras = true;
	public bool make_camera = false; // Generate our own camera if the user wants.
	public bool check_main_cam_active = false;

	public float player_forward_mult = 1.0f;
	public float player_sideways_mult = 1.0f;
	public float player_backwards_mult = 1.0f;

	public float player_jump_height = 1.0f;

	public float grounded_check_distance = 1.3f;

	public float jump_delay = 0.3f;

	public Vector3 camera_lookat_offset = new Vector3(0.0f,0.7f,0.0f);

	public float scroll_speedmult = 1.0f;

	public float default_zoom = 10.0f; //default camera distance from player.

	public float camera_min_zoom = 3.0f;
	public float camera_max_zoom = 20.0f;

	public float camera_x_default = 45.0f;

	public float mouse_speed_mult_x = 4.0f;
	public float mouse_speed_mult_y = -4.0f;

	public float maxCamRotX = 85.0f;
	public float minCamRotX = 0.0f;

	public float camera_snap_speedmult = 0.6f;

	public float camera_rot_offset_min = 5.0f; //degrees.

	public bool showMouse = false;

	//Mouse stuff

	private float mouseX = 0.0f;
	private float mouseY = 0.0f;

	//we're going to be working with the standard globe-controls, where the camera rotates in X and Y coordinates around the player, (up-down and left-right), with zoom.
	private float camera_zoom; // this is going to change a lot.

	private float camera_rot_x = 0.0f; //-90 to +90. 0 will be horizontal. Rotates along x-axis
	private float camera_rot_y = 0.0f; //-180 to 180. 0 is forward.
	private float camera_rot_x_offset = 0.0f;
	private float camera_rot_y_offset = 0.0f;

	private Vector3 camera_pos = new Vector3(); //relative to the player position
	private Vector3 camera_offset;
	private Vector3 camera_distance_offset;

	private bool combat_mode = false; //whether or not the player is always facing the direction of the camera, or only when we press w.

	private bool player_sub_camera = false; //when we press w, the player moves forwards. This decides whether or not the players orientation is the same as the camera y rotation.
	//w&!right_click then true, w&&right_click then false, !w&&right_click then true, !w&!right_click then false
	//w is w key

	private bool player_sub_camera_temp_offset = false;

	//player phsyics:

	private Vector3 player_velocity = new Vector3();
	private Vector3 player_force = new Vector3();

	private Transform player_transform;

	private Rigidbody player_rb;

	private bool grounded = false;
	private bool jumping = false;

	private float jump_timer = 0.0f;

	private float tempTimerTest = 0.0f;

	private GameObject textBox;

	private Text printBox;

	//using "gameObject" Will reference whatever we put this script in.

	// Init
	void Start () {

		textBox = GameObject.Find ("txtInfo");

		printBox = textBox.GetComponent<Text> ();

		printBox.text += "Hello, World!";

		if (camera_snap_speedmult > 0.9f || camera_snap_speedmult < 0.1f) {

			print ("Camera snap speedmult MUST be between 0.1 and 0.9. Resetting to default.");
			camera_snap_speedmult = 0.5f;

		}

		print ("Old Gravity: " + Physics.gravity.ToString ());
		Physics.gravity = new Vector3 (0.0f, -9.8f*2.0f, 0.0f);
		print ("New Gravity: " + Physics.gravity.ToString ());

		player_transform = gameObject.GetComponent<Transform> ();

		player_rb = gameObject.GetComponent<Rigidbody> ();

		if (showMouse == false) {

			Cursor.visible = false;

		}

		//camera set-up in here, if any.
		camera_distance_offset = new Vector3(0.0f,0.0f,camera_zoom);
		camera_zoom = default_zoom;
		camera_rot_x = camera_x_default;

		//camera_offset = new Vector3 (0.0f, (Mathf.Sin (camera_rot_x) * camera_zoom), (Mathf.Cos (camera_rot_x) * camera_zoom));
		//camera_offset = Quaternion.AngleAxis (camera_rot_y, Vector3.up) * camera_offset;

		//rotate the camera
		camera_offset = Quaternion.Euler (-1.0f * camera_rot_x, camera_rot_y + 180.0f, 0.0f) * camera_distance_offset;

		//position the camera
		camera_pos = gameObject.transform.position + camera_offset;
		//rotate the camera around the Y-axis.

		//check to see if the camera is active and enabled.
		if (check_main_cam_active) {
			
			if (!main_camera.isActiveAndEnabled) {

				main_camera.gameObject.SetActive (true);
				main_camera.enabled = true;

			}
		}

		if (turn_off_other_cameras) {
			//we're going to check for and turn off other cameras for now. We wont be looking through them, and we don't have any reason to draw everything twice right now. 
			print ("Number_of_cameras: " + Camera.allCameras.Length.ToString ());

			if (Camera.allCameras.Length > 1) {
				//run through all of the cameras in the scene
				for (int i = 0; i < Camera.allCameras.Length; i++) {

					if (Camera.allCameras [i] != main_camera) {

						Camera.allCameras [i].enabled = false;
						Camera.allCameras [i].gameObject.SetActive (false);

					}

				}

			}

		}

	}
	
	// Update is called once per frame
	void Update () {
		//camera controls in here.
		//we want to position the camera based on the variables each frame.

		camera_distance_offset.z = camera_zoom;

		//rotate the camera
		camera_offset = Quaternion.Euler (-1.0f * (camera_rot_x+camera_rot_x_offset), (camera_rot_y+camera_rot_y_offset) + 180.0f, 0.0f) * camera_distance_offset;

		//position the camera
		camera_pos = gameObject.transform.position + camera_offset;

		//set final camera pos
		main_camera.gameObject.transform.position = camera_pos;

		//call this AFTER we calculate the cameras new location
		main_camera.gameObject.transform.LookAt(gameObject.transform.position + camera_lookat_offset);


	}

	// FixedUpdate is called once every time physics are calculate. Basically a "physics" 
	void FixedUpdate () {



		/*
		 *Rules:
		 *We want to be able to rotate the camera around the player. (moving the camera)
		 *We want the camera to focus on the players head.
		 *We want to be able to zoom the camera. (scroll wheel)
		 *We want the player to turn and run in the direction that the camera is pointing when we press w. 
		 *When we are running, we want to press right click and move the camera without changing player direction.
		 *Switch to combat mode when we press left-click, or press a key. Maybe toggle out of it 
		 *
		 */

		//-----------------------------
		//Camera rotation
		mouseX = Input.GetAxis("Mouse X");
		mouseY = Input.GetAxis("Mouse Y");

		if (Input.GetAxisRaw ("Fire2") == 0) {
			//rotate around y axis without mouse X, and vice versa
			camera_rot_x += mouseY * mouse_speed_mult_x;
			camera_rot_y += mouseX * mouse_speed_mult_y;

			if (camera_rot_x >= maxCamRotX) {

				camera_rot_x = maxCamRotX;

			} else if (camera_rot_x <= minCamRotX) {

				camera_rot_x = minCamRotX;

			}

		} else {

			camera_rot_x_offset += mouseY * mouse_speed_mult_x;
			camera_rot_y_offset += mouseX * mouse_speed_mult_y;

			if (camera_rot_x+camera_rot_x_offset >= maxCamRotX) {

				camera_rot_x_offset = maxCamRotX-camera_rot_x;

			} else if (camera_rot_x+camera_rot_x_offset <= minCamRotX) {

				camera_rot_x_offset = minCamRotX-camera_rot_x;

			}

		}



		//-----------------------------
		//Camera zoom

		camera_zoom += Input.GetAxis ("Mouse ScrollWheel") * scroll_speedmult * -1.0f;
		if (camera_zoom >= camera_max_zoom) {

			camera_zoom = camera_max_zoom;

		} else if (camera_zoom <= camera_min_zoom) {

			camera_zoom = camera_min_zoom;

		}

		//-----------------------------
		//Player control

		//player_sub_camera decides whether or not the player turns in the direciton of the camera
		if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Fire1") == 0) {


			//Release a control key while camera offset is active. Set camera rot to offset and reset offset.
			player_sub_camera = false;
			player_sub_camera_temp_offset = false;
			camera_rot_x = camera_rot_x + camera_rot_x_offset;
			camera_rot_y = camera_rot_y + camera_rot_y_offset;

			camera_rot_x_offset = 0;
			camera_rot_y_offset = 0;

		} else if (player_sub_camera_temp_offset == false) {

			player_sub_camera = true;

		}

		//if right mouse is pressed, start the state of viewing the player while running
		if (Input.GetAxisRaw("Fire2") != 0){

			player_sub_camera = false;

			player_sub_camera_temp_offset = true;

		}

		if (player_sub_camera == true && player_sub_camera_temp_offset == false) {

			transform.rotation = Quaternion.AngleAxis (camera_rot_y, Vector3.up);

		} else if (player_sub_camera_temp_offset && Input.GetAxisRaw("Fire2") == 0) {
			//Released fire2, put the camera back
			//we want to rotate the camera back smoothly

			//reduce camera x offset
			if (Mathf.Abs (camera_rot_x_offset) > camera_rot_offset_min ) {

				camera_rot_x_offset = camera_rot_x_offset * camera_snap_speedmult;

			} else {

				camera_rot_x_offset = 0.0f;

			}

			//reduce camera y offset
			if (Mathf.Abs (camera_rot_y_offset) > camera_rot_offset_min ) {

				camera_rot_y_offset = camera_rot_y_offset * camera_snap_speedmult;

			} else {

				camera_rot_y_offset = 0.0f;

			}

			if (camera_rot_x_offset == 0.0f && camera_rot_y_offset == 0.0f) {

				player_sub_camera_temp_offset = false;

			}


		}

		if (player_sub_camera_temp_offset == true) {
			//we want to do some limit checks.

			if (camera_rot_y_offset + camera_rot_y > (camera_rot_y+180.0f)) {
				
				camera_rot_y_offset = camera_rot_y_offset - 360.0f;

			}

			if (camera_rot_y_offset + camera_rot_y < (camera_rot_y-180.0f)) {

				camera_rot_y_offset = camera_rot_y_offset + 360.0f;


			}


		}

		//player move and jump:

		player_velocity = Vector3.zero; //reset velocity each time
		player_force = Vector3.zero;

		if (Input.GetAxis ("Vertical") > 0) {

			player_velocity.z += Input.GetAxis ("Vertical") * player_forward_mult;

		} else if (Input.GetAxis ("Vertical") < 0) {

			player_velocity.z += Input.GetAxis ("Vertical") * player_backwards_mult;

		}

		player_velocity.x += Input.GetAxis ("Horizontal") * player_sideways_mult;

		player_velocity = player_transform.rotation * player_velocity;

		player_rb.MovePosition (player_transform.position  + player_velocity);
		//+= transform.rotation * player_velocity;

		//jump stuff

		//use a raycast to check if we're close to the ground
		grounded = Physics.Raycast (transform.position, Vector3.down, grounded_check_distance);

		if (jump_timer > 0) {

			jump_timer = jump_timer - Time.fixedDeltaTime;

		}

		if (jump_timer < 0) {

			jump_timer = 0;

		}

		if (Input.GetAxisRaw ("Jump") > 0 && grounded && jump_timer == 0) {

			player_force.y = player_jump_height + (0.0f - player_rb.velocity.y);

			jump_timer = jump_delay;

		}

		player_rb.velocity += player_force;

		printBox.text = "Sub_camera_offset status: " + player_sub_camera_temp_offset.ToString ()
			+ "\nplayer_sub_camera: " + player_sub_camera.ToString ()
			+ "\ncamera_rot_x_offset: " + camera_rot_x_offset.ToString()
			+ "\ncamera_rot_y_offset: " + camera_rot_y_offset.ToString()
			+ "\ncamera_rot_x: " + camera_rot_x.ToString()
			+ "\ncamera_rot_y: " + camera_rot_y.ToString();
		
	}

}
