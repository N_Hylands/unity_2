﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour {

	public GameObject bullet;

	public float delay = 0.1f;

	//private variables

	private float timer = 0.0f;

	private bool shooting = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//fixedupdate callsed once per physics frame
	void FixedUpdate(){

		if (Input.GetAxisRaw ("Fire1") != 0 && shooting == false) {

			Instantiate (bullet, transform.position, transform.rotation);

			shooting = true;

		}

		if (shooting) {

			timer += Time.fixedDeltaTime;

		}

		if (timer > delay) {

			timer = 0.0f;
			shooting = false;

		}

	}
}
