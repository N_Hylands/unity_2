﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseAxisTest : MonoBehaviour {

	private float mouseY;
	private float mouseX;

	private Text mytext;

	// Use this for initialization
	void Start () {
		mytext = gameObject.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		mouseX = Input.GetAxis("Mouse X");
		mouseY = Input.GetAxis("Mouse Y");

		mytext.text = "X: " + mouseX.ToString () + "\nY: " + mouseY.ToString () + "\n Vertical Input";

		mytext.text += "\n vertical axis: " + Input.GetAxis ("Vertical").ToString ();

		mytext.text += "\n horizontal axis: " + Input.GetAxis ("Horizontal").ToString ();

		mytext.text += "\nRaw scrollwheel: " + Input.GetAxisRaw ("Mouse ScrollWheel").ToString() + "\nScrollwheel: " + Input.GetAxis("Mouse ScrollWheel").ToString();
	}
}
