﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
	//bullet script

	public float speed = 100.0f;

	private Vector3 speedVector;

	private float timer;

	private Rigidbody rb;


	// Use this for initialization
	void Start () {
		speedVector = new Vector3(0.0f,0.0f,speed);

		rb = gameObject.GetComponent<Rigidbody> ();

		rb.velocity = transform.rotation * (new Vector3 (Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")) * 10.0f);

		//print (GameObject.FindGameObjectWithTag ("Player").GetComponent<Rigidbody> ().velocity.ToString ());

		GameObject.Destroy (gameObject, 2);

		//speedVector = transform.rotation * speedVector;
		print (rb.velocity.ToString());

	}
	
	// Update is called once per frame
	void Update () {
		


	}
	void FixedUpdate(){
		
		rb.AddRelativeForce (speedVector);

	}
}
